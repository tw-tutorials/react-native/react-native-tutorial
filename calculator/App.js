import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ToastAndroid, Vibration } from 'react-native';

export default class App extends Component {

    constructor() {
        super();
        this.state = {
            resultText: '',
            calculationText: ''
        };

        this.operations = ['DEL', '+', '-', '*', '/'];
    }

    calculateResult() {
        const text = this.state.resultText;

        let result = eval(text);
        this.setState({
            resultText: '',
            calculationText: result
        });
    }

    buttonPressed(text) {
        if (text == '=') {
            this.calculateResult();
            return;
        }

        this.setState({
            resultText: this.state.resultText + text
        });
    }
    operate(operation) {
        const { resultText, calculationText } = this.state;

        switch (operation) {
            case 'DEL':
                const text = resultText.split(''); // split after each character
                text.pop(); // remove last element
                this.setState({ resultText: text.join('') });
                break;

            case '+':
            case '-':
            case '*':
            case '/':
                if (resultText == '') {
                    if (calculationText == '') return;
                    else {
                        this.setState({ resultText: calculationText + '' + operation });
                        return;
                    }
                }

                const lastChar = resultText.split('').pop();
                if (this.operations.indexOf(lastChar) > 0) return;

                this.setState({ resultText: resultText + operation });
                break;

            default:
                break;
        }
    }

    clearResultText() {
        this.setState({ resultText: '', calculationText: '' });
        Vibration.vibrate(200);
    }



    render() {

        let nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9], ['.', 0, '=']]
        let rows = [];
        for (let i = 0; i < 4; i++) {
            let row = [];
            for (let j = 0; j < 3; j++) {
                row.push(<TouchableOpacity key={'btn_' + +i + '_' + j} style={styles.btn} onPress={() => this.buttonPressed(nums[i][j])}>
                    <Text style={styles.btnText}>{nums[i][j]}</Text>
                </TouchableOpacity>);
            }
            rows.push(<View key={'row_' + i} style={styles.row}>{row}</View>)
        }


        ops = [];
        for (let i = 0; i < this.operations.length; i++) {
            if (this.operations[i] == 'DEL') {
                ops.push(
                    <TouchableOpacity key={'op_' + i} style={styles.btn} onPress={() => this.operate(this.operations[i])} onLongPress={() => this.clearResultText()}>
                        <Text style={[styles.btnText, styles.white]}>{this.operations[i]}</Text>
                    </TouchableOpacity>
                );
            } else {
                ops.push(
                    <TouchableOpacity key={'op_' + i} style={styles.btn} onPress={() => this.operate(this.operations[i])}>
                        <Text style={[styles.btnText, styles.white]}>{this.operations[i]}</Text>
                    </TouchableOpacity>
                );
            }
        }

        return (
            <View style={styles.container} >
                <View style={styles.result}>
                    <Text style={styles.resultText}>{this.state.resultText}</Text>
                </View>
                <View style={styles.calculation}>
                    <Text style={styles.calculationText}>{this.state.calculationText}</Text>
                </View>
                <View style={styles.buttons}>
                    <View style={styles.numbers}>
                        {rows}
                    </View>
                    <View style={styles.operations}>
                        {ops}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    white: {
        color: 'white'
    },

    result: {
        flex: 2,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    resultText: {
        fontSize: 30,
        color: '#202124'
    },

    calculation: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    calculationText: {
        fontSize: 24,
        color: '#80868b'
    },

    buttons: {
        flex: 7,
        flexDirection: 'row'
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        alignSelf: 'stretch',
        justifyContent: 'center',
    },
    btnText: {
        fontSize: 35,
        color: '#1F1F20'
    },
    numbers: {
        flex: 3,
        backgroundColor: '#f1f3f4',
    },
    operations: {
        flex: 1,
        justifyContent: 'space-around',
        backgroundColor: '#1a73e8'
    }
});